#!/bin/sh
#
# This file is part of the Universal Package Manger by Ethan Ruffing.
# Copyright 2015-2016 by Ethan Ruffing. All rights reserved. See the
# accompanying NOTICE.md.
#
# Outputs help text for the Universal Package Manager (UPM)

echo Universal Package Manager
echo =========================
echo ---------Help------------
